SHELL = /bin/sh
export ROOTS_VERSION=master

export COMPANY_NAME=SkaleSys
export COMPANY_WEBSITE=https://skalesys.com

export LOGO_THEME=smile
export PROJECT_NAME=terraform-comprehend

export REGION=ap-southeast-2
export BUCKET=sk-prd-terraform-state
export DYNAMODB_TABLE=sk-prd-terraform-state-lock

SAMPLE_NUMBER?=1

-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)

.PHONY: install
## Install project requirements
install:
	@make --silent terraform/install
	@make --silent gomplate/install

.PHONY: analysis/start
## Starts a sentiment analysis
analysis/start:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws comprehend start-sentiment-detection-job --cli-input-json file://cli-input.json \
	)

.PHONY: send/sample
send/sample:
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate && \
		aws kinesis put-record --stream-name sk-dev-analysis-stream --data '$(shell base64 samples/$(SAMPLE_NUMBER).txt)' --partition-key $(shell uuidgen) \
	)

.PHONY: send/samples
send/samples:
	for number in 1 2 3 4 5; do \
		make send/sample SAMPLE_NUMBER=$$number ; \
	done